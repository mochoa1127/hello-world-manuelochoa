<?php

namespace App\Http\Controllers;

use App\Message;

use Illuminate\Http\Request;

class MessageController extends Controller
{
    public function index(Request $request)
    {
        $messages = Message::all();

        return $messages;

    }

    public function render()
    {
        $message = Message::all()->first();
        return view('welcome')->with('message', $message);

    }

    public function show(Message $message)
    {
        return $message;

    }
}
