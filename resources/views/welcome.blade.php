<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Aliados Web SAS</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.6.2/css/bulma.min.css">
    <script defer src="https://use.fontawesome.com/releases/v5.0.0/js/all.js"></script>

</head>
<body>
<section class="hero is-primary">
    <div class="hero-body">
        <div class="container">
            <h1 class="title">
                {{$message->text}}
            </h1>
            <h2 class="subtitle">
                Message from Database
            </h2>
            <a href="/api/messages" class="button is-outlined">All messages</a>
        </div>
    </div>
</section>
<section class="section">
    <div class="container">
        <table class="table">
            <thead>
            <tr>
                <th>ID</th>
                <th>Text</th>
                <th>Created At</th>
                <th>Updated At</th>
            </thead>
            </tr>
            <tbody>
            <tr>
                <td>
                    <a href="{{ URL::to('/api/messages/' . $message->id) }}" title="All Messages">{{$message->id}}</a>
                </td>
                <td>{{$message->text}}</td>
                <td>{{$message->created_at}}</td>
                <td>{{$message->updated_at}}</td>
            </tr>
            </tbody>

        </table>
    </div>
</section>
</body>
</html>
