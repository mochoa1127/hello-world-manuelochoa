<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class messagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('messages')->insert([
            'text' => 'Hello world',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
    }
}
