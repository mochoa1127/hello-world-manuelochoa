<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::middleware('cors')->group(function () {
    Route::get('/messages', 'MessageController@index');
    Route::get('/messages/{message}', 'MessageController@show');
});


Route::get('/health', function(){
    return 'healthy';
});
